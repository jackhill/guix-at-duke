(define-module (guix-at-duke services endpoint-management)
  #:use-module (gnu packages certs)
  #:use-module (gnu services mcron)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (guix-at-duke packages endpoint-management)
  #:export (planisphere-report-configuration
	    planisphere-report-configuration?
	    planisphere-report-service-type))

(define-record-type* <planisphere-report-configuration>
  planisphere-report-configuration make-planisphere-report-configuration
  planisphere-report-configuration?
  (certs planisphere-report-certs
	 (default nss-certs))
  (package planisphere-report-package
	   (default planisphere-report))
  (schedule planisphere-report-schedule
	    (default '(quote (next-minute-from (next-hour '(0 12)) (list (random 60)))))))

(define (planisphere-report-jobs config)
  (let ((sched-spec (planisphere-report-schedule config))
	(certs (file-append (planisphere-report-certs config) "/etc/ssl/certs"))
	(cmd (file-append (planisphere-report-package config) "/bin/planisphere-report")))
    (list
     #~(append-environment-mods "SSL_CERT_DIR" #$certs)
     #~(job #$sched-spec #$cmd))))

(define planisphere-report-service-type
  (service-type
   (name 'planisphere-report)
   (extensions
    (list (service-extension mcron-service-type planisphere-report-jobs)))
   (default-value (planisphere-report-configuration))
   (description "Periodically run @command{planisphere-report} to update the OIT endpoint database.")))
