(define-module (guix-at-duke packages endpoint-management)
  #:use-module (gnu packages)
  #:use-module (gnu packages autotools)
  #:use-module ((gnu packages base) #:select (coreutils))
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages guile-xyz)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages texinfo)
  #:use-module (guix build utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system trivial)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages))

(define-public guix-installed
  (let ((commit "5c1a589fdc58a7a2f38968c89b77727228c317f7")
	(revision "3"))
    (package
     (name "guix-installed")
     (version (git-version "0.0.1" revision commit))
     (source (origin
	      (method git-fetch)
	      (uri
	       (git-reference
		(url "https://gitlab.oit.duke.edu/jackhill/guix-installed")
		(commit commit)))
	      (file-name (git-file-name name version))
	      (sha256
	       (base32 "1pgq214jw6xl1f6nd8wmhpkw86mhnk96dpwnrj5jyg76857qfnh7"))))
     (build-system gnu-build-system)
     (arguments
      `(#:modules ((ice-9 match) (ice-9 ftw)
                  ,@%gnu-build-system-modules)
	#:phases
	(modify-phases %standard-phases
	  (add-after 'unpack 'patch-ls-path
	    (lambda _
	      (substitute* "guix-installed.scm"
		(("ls -d") (string-append (which "ls") " -d")))))
	  (add-before 'bootstrap 'hall-dist
	    (lambda _
	      (begin (setenv "HOME" "/tmp")
		     (invoke "hall" "build" "-x"))))
	  (add-after 'install 'wrap-guix-installed
            (lambda* (#:key inputs outputs #:allow-other-keys)
              ;; Wrap the 'guix-installed' command to refer to the right
              ;; modules. Adapted from the huant package
              (let* ((out  (assoc-ref outputs "out"))
                     (bin  (string-append out "/bin"))
                     (site (string-append
                            out "/share/guile/site"))
                     (deps (list (assoc-ref inputs "guix")
				 (assoc-ref inputs "guile-gcrypt"))))
                (match (scandir site)
                  (("." ".." version)
                   (let ((modules (string-append site "/" version))
                         (compiled-modules (string-append
                                            out "/lib/guile/" version
                                            "/site-ccache")))
                     (wrap-program (string-append bin "/guix-installed")
                       `("GUILE_LOAD_PATH" ":" prefix
                         (,modules
                          ,@(map (lambda (dep)
                                   (string-append dep
                                                  "/share/guile/site/"
                                                  version))
                                 deps)))
                       `("GUILE_LOAD_COMPILED_PATH" ":" prefix
                         (,compiled-modules
                          ,@(map (lambda (dep)
                                   (string-append dep "/lib/guile/"
                                                  version
                                                  "/site-ccache"))
                                 deps))))
                     #t)))))))))
     (native-inputs
      (list autoconf automake guile-hall pkg-config texinfo))
     (inputs
      (list coreutils ;needed for ls
            guile-3.0
            guile-gcrypt ;needed as transitive dependency of guix
            guix))
     (home-page "https://gitlab.oit.duke.edu/jackhill/guix-installed")
     (synopsis "Tool to list installed packages for GNU Guix")
     (description "Lists the installed package for GNU Guix in a format that is 
suitable to be reported to planisphere.")
     (license license:gpl3+))))

(define-public planisphere-report
  (let ((commit "1b606af0d5f1f42ed0808759278c3b0571a12f4f")
	(revision "0"))
      (package
	(name "planisphere-report")
	(version (git-version "0" revision commit))
	(source (origin
		  (method git-fetch)
		  (uri (git-reference
			(url "https://gitlab.oit.duke.edu/duke-guix/planisphere-tools.git")
			(commit commit)))
		  (sha256
		   (base32 "1lr9dmvvamaz00a2hi3f642pp2y3rr22zs6rmnfmlhs3xjdsf62a"))
		  (file-name (git-file-name name version))))
	(build-system trivial-build-system)
	(arguments
	 `(#:modules ((guix build utils))
	   #:builder
	   (begin
	     (use-modules (guix build utils))
	     (let ((source (assoc-ref %build-inputs "source"))
		   (guix-installed (assoc-ref %build-inputs "guix-installed"))
		   (python-2 (assoc-ref %build-inputs "python-2"))
		   (script "planisphere-report")
		   (out (assoc-ref %outputs "out")))
	       (copy-file (string-append source "/" script) script)
	       (substitute* script
	       	 (("/usr/bin/env python2") (string-append python-2 "/bin/python2"))
	       	 (("guix-installed") (string-append guix-installed "/bin/guix-installed"))
		 (("/etc/os-release") (string-append out "/etc/os-release")))
	       (call-with-output-file "os-release"
		 (lambda (output-port)
		   (display "PRETTY_NAME=guix\n" output-port)))
      	       (install-file script (string-append out "/bin"))
	       (install-file "os-release" (string-append out "/etc"))))))
	(inputs
	 `(("guix-installed" ,guix-installed)
	   ("python-2" ,python-2)))
	(home-page "https://gitlab.oit.duke.edu/duke-guix/planisphere-tools")
	(synopsis "Report host information to Planisphere")
	(description "Self-reporting tool for Planisphere that fulfills the
endpoint management requirement.")
	(license license:expat))))
