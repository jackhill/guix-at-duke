(define-module (guix-at-duke packages utils)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages ssh)
  #:use-module (guix build-system trivial)
  #:use-module (guix download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages))

(define-public duo-scp
  (package
   (name "duo-scp")
   (version "0")
   (source
    (origin
     (method url-fetch)
     (uri "https://gitlab.oit.duke.edu/snippets/82/raw")
     (sha256
      (base32 "1mrf5451cj8010iq76866dli1hxa31rk8i993arf9dfyzws26v2b"))
     (file-name name)))
   (build-system trivial-build-system)
   (arguments
    `(#:modules ((guix build utils))
      #:builder
      (begin
	(use-modules (guix build utils))
	(let ((source (assoc-ref %build-inputs "source"))
	      (ssh (assoc-ref %build-inputs "openssh"))
	      (bash (assoc-ref %build-inputs "bash"))
	      (script "duo-scp")
	      (out (assoc-ref %outputs "out")))
	  (copy-file source script)
	  (substitute* script
		       (("/bin/bash") (string-append bash "/bin/bash"))
		       (("scp") (string-append ssh "/bin/scp")))
	  (chmod script #o555)
	  (install-file script (string-append out "/bin"))))))
   (inputs
    (list bash openssh))
   (home-page "https://gitlab.oit.duke.edu/snippets/82/raw")
   (synopsis "User Duo Passcode with scp and login_duo")
   (description "Wrapper script for scp that prompts for a passcode
before calling scp.  This allows the user to supply a passcode from e.g. a Yubikey
to avoid getting a Duo push or phone call when talking to an ssh server that uses
the login_duo script for multifactor.")
   (license license:cc0)))
